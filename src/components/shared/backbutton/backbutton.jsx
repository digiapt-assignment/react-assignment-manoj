import React from 'react';
import {Link} from "react-router-dom";
import "./backbutton.scss";

class Back extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <div className="back-button__root">
                <Link to={this.props.to}><i class="fa fa-chevron-left"></i>&nbsp;Back</Link>
            </div>
        );
    }
}

export default Back;
