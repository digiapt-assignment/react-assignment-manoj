export const menuDataUser = [
    {
        value: "dashboard",
        label:"Dashboard",
        icon: "fa fa-home"
    },
    {
        value: "capsules",
        label:"Capsules",
        icon: "fa fa-home"
    },
    {
        value: "cores",
        label:"Core",
        icon: "fa fa-home"
    },
    {
        value: "dragons",
        label:"Dragons",
        icon: "fa fa-home"
    },
]

export const menuDataAdmin = [
    {
        value: "dashboard",
        label:"Dashboard",
        icon: "fa fa-home"
    },
    {
        value: "capsules",
        label:"Capsules",
        icon: "fa fa-home"
    },
    {
        value: "cores",
        label:"Core",
        icon: "fa fa-home"
    },
    {
        value: "dragons",
        label:"Dragons",
        icon: "fa fa-home"
    },
    {
        value: "history",
        label:"History",
        icon: "fa fa-home"
    },
    {
        value: "landpads",
        label:"Landing pads",
        icon: "fa fa-home"
    },
    {
        value: "launches",
        label:"Launches",
        icon: "fa fa-home"
    },
    {
        value: "launchpads",
        label:"Launch pads",
        icon: "fa fa-home"
    },
    {
        value: "missions",
        label:"Missions",
        icon: "fa fa-home"
    },
    {
        value: "payloads",
        label:"Payloads",
        icon: "fa fa-home"
    },
    {
        value: "rockets",
        label:"Rockets",
        icon: "fa fa-home"
    },
    {
        value: "roadster",
        label:"Roadster",
        icon: "fa fa-home"
    },
    {
        value: "ships",
        label:"Ships",
        icon: "fa fa-home"
    },
]